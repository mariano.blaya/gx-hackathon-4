__author__ = "Jan Frömberg"
__copyright__ = "Copyright 2022, Cloud and Heat Technologies GmbH"
__credits__ = ["Jan Frömberg"]
__license__ = "Apache License 2.0"
__version__ = "1.0.0"
__email__ = "jan.froemberg@cloudandheat.com"

import json
import os
import fnmatch

""" 
writes the given json data to a given json file
"""
def writeJsonToFile(file, data):
    with open('../Example Self-Descriptions/' + file, "w") as write_file:
        json.dump(data, write_file)

""" 
gets all json files from the given folder and returns that file list
"""
def getSDFilesFromFolder(path):
    # filter for json files
    files = fnmatch.filter(os.listdir(path), "*.json")
    # return file list
    return files

""" 
reads a self-description json file
"""
def readSelfDescription(file):

    # read file
    with open('../Example Self-Descriptions/'+ file, 'r') as sdfile:
        data = sdfile.read()

    # parse file
    obj = json.loads(data)

    return obj