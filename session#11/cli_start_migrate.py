### Gaia-x Hackthon #4 Session #11
### The CLI start script for an Openstack migration 
###
__author__ = "Jan Frömberg"
__copyright__ = "Copyright 2022, Cloud and Heat Technologies GmbH"
__credits__ = ["Jan Frömberg"]
__license__ = "Apache License 2.0"
__version__ = "1.0.0"
__maintainer__ = "Jan Frömberg"
__email__ = "jan.froemberg@cloudandheat.com"

import openstack

from OpenStackServices import *
from SelfDescriptionReader import *

"""
cli start script, please use: python3 cli_start_migrate.py
"""
if __name__ == "__main__":

    print('')
    ### testing sd access can be removed
    sdfiles = getSDFilesFromFolder("../Example Self-Descriptions")
    for file in sdfiles:
        print("collected sd files: " + file)
    
    f = readSelfDescription("provider.json")
    # example sd file contend to be used in the migration process
    print('contend of a given sd-file: ' + f["name"] + " " + f["contact"][0]["value"] )
    ### end of remove

    print('')
    print("---------------------------Start Migration---------------------------------")
    print('')

    # please do comment out migrations not needed
    BlockStorageMigration("glance").migrate()
    NetworkMigration("neutron").migrate()
    ComputeMigration("nova").migrate()
    IdentityMigration("keystone").migrate()
    ContinerInfraMigration("container").migrate()
    VolumeMigration("vol").migrate()
    LoadBalancerMigration("octavia").migrate()
    KeyManagerMigration("barbican").migrate()
