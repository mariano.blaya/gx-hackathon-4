##
# This is the openstack connection class
#
__author__ = "Jan Frömberg"
__copyright__ = "Copyright 2022, Cloud and Heat Technologies GmbH"
__credits__ = ["Jan Frömberg"]
__license__ = "Apache License 2.0"
__version__ = "1.0.0"
__email__ = "jan.froemberg@cloudandheat.com"


import openstack


"""
Initialize a os connection by a given name 'mycloud'
see clouds.yaml file for configuration
"""
class Connection:

    def __init__(self, mycloud) -> None:
        # Initialize and turn on debug logging
        openstack.enable_logging(debug=True)

        # Initialize connection
        self.conn = openstack.connect(cloud=mycloud)
    
    """
    return the connection
    """
    def getCon(self) -> openstack.connection.Connection :
        return self.conn

    """
    close the connection
    """
    def close(self):
        self.conn.close()

    """
    list all servers of that given connection
    """
    def listServers(self):
        # List the servers
        for server in self.conn.compute.servers():
            print(server.to_dict())